body{
    background-image: url('bkground.png');
    background-size: cover;
    background-position: center;
    background-repeat: repeat;
}

.my-image{
    border-radius: 100%;
    margin-top: 10%;
    margin-left: 17%;
}

.my-info{
    margin-top: -10%;
    margin-left: 74%;
    width: 20%;
    height: 20%;
}

.my-educ{
    margin-top: 23%;
    margin-left: 4%;
    width: 20%;
    height: 20%;
}

.my-inte{
    margin-top: 23%;
    margin-left: 74%;
    width: 20%;
    height: 20%;
}

.fb{
    margin-top: -87%;
    margin-left: 5%;
    position:absolute;
    width: 5%;
    height: 5%;
}

.twitter{
    margin-top: -82%;
    margin-left: 5%;
    position:absolute;
    width: 5%;
    height: 5%;
}

.youtube{
    margin-top: -77%;
    margin-left: 5%;
    position:absolute;
    width: 5%;
    height: 5%;
}

#main h1{
    margin-top: 5%;
    margin-left: 37%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 70px;
    font-family: 'aliens and cows';
}

#main h4{
    background-color: rgba(255, 253, 253, 0.623);
    padding: 6%;
    border-radius: 15px;
    width: 500px;
    height: 280px;
    margin-top: 10%;
    margin-left: 50%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 21px;
    font-family: 'abeatbyKai';
}

#education h4{
    background-color: rgba(255, 253, 253, 0.623);
    padding: 6%;
    border-radius: 15px;
    width: 500px;
    height: 280px;
    margin-top: 43%;
    margin-left: 1.5%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 21px;
    text-align: right;
    font-family: 'abeatbyKai';
}

#interest h4{
    background-color: rgba(255, 253, 253, 0.623);
    padding: 6%;
    border-radius: 15px;
    width: 500px;
    height: 280px;
    margin-top: 76%;
    margin-left: 50%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 21px;
    font-family: 'abeatbyKai';
}

#work h4{
    margin-top: 81%;
    margin-left: 24%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 21px;
    text-align: right;
    font-family: 'abeatbyKai';
}
#qoute h4{
    margin-top: 48%;
    margin-left: 55%;
    color:rgb(55, 103, 143);
    position: absolute;
    font-size: 21px;
    font-family: 'abeatbyKai';
}

